#!/usr/bin/env bash

# Purpose: This script builds a chart tied to a specific service and version based on a generic template.
# INPUTS: 
#   SERVICE_NAME - This is the name of the chart to be built. It should be the name of the service the chart is for.
#   VERSION - This is the version of the chart to be built. 

SERVICE_NAME=$1
VERSION=$2

echo $SERVICE_NAME
echo $VERSION

mkdir -p generated-charts
cp -rf ./ncid-generic ./generated-charts/$SERVICE_NAME:$VERSION

cat > ./generated-charts/$SERVICE_NAME:$VERSION/Chart.yaml << EOF
apiVersion: v1
name: $SERVICE_NAME
description: A helm chart for $SERVICE_NAME
type: application

version: $VERSION
appVersion: $VERSION
EOF

cd generated-charts

#helm package ./$SERVICE_NAME:$VERSION --debug

