#!/usr/bin/env bash

# Purpose: This script deploys the ncid-init chart to a specific environment
# Inputs: 
#   NAMESPACE - This is the namespace where the chart will be deployed.
#   APIKEY - This is the Artifactory API key.

NAMESPACE=$1
VERSION=$2
APP_NAME=$3
IMAGE_NAME=$4

# upgrade chart
helm upgrade $APP_NAME ./generated-charts/$APP_NAME:$VERSION \
    --version $VERSION --install --wait --debug \
    --namespace $NAMESPACE \
    --set image.name=$IMAGE_NAME \
#    --set image.tag=$VERSION \
#    --set imagePullSecrets=ibm-cr-secret \
#    --set deploy.volumeMounts[0].name=$APP_NAME-volume-map,deploy.volumeMounts[0].mountPath=/usr/src/app/config/default.json,deploy.volumeMounts[0].subPath=default.json \
#    --set deploy.volumes[0].name=$APP_NAME-volume-map,deploy.volumes[0].configMap.defaultMode=493,deploy.volumes[0].configMap.name=$APP_NAME-$NAMESPACE-config \
#    --set deploy.livenessProbe=null \
#    --set deploy.readinessProbe.httpGet=null

