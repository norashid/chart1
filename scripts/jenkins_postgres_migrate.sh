#!/usr/bin/env bash

# Purpose: This script install npm and runs commands to migrate a postgres db. It assumes that all required environment variables have been set.

# install node

# curl -sL https://deb.nodesource.com/setup_12.x | sudo bash - && sudo apt-get install -yq nodejs && sudo npm install -g npm && sudo rm -rf ~/.npm /tmp/npm-*

npm ci
npm run db:migrate
