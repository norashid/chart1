#!/usr/bin/env bash

# Purpose: This script deploys a specific chart to the requested namespace from the ncid artifactory helm repo.
# Inputs: 
#   SERVICE_NAME - This is the name of the service to be deployed.
#   VERSION - This is the version of the chart to be deployed. 
#   NAMESPACE - This is the namespace where the chart will be deployed.
#   USERNAME - This is the User name for Artifactory.
#   APIKEY - This is the Artifactory API key.

SERVICE_NAME=$1
VERSION=$2
NAMESPACE=$3
# USERNAME=$4
# APIKEY=$5

# helm3 repo add gbs-ncid-tech-team-helm-virtual https://na.artifactory.swg-devops.com/artifactory/gbs-ncid-tech-team-helm-virtual --username $USERNAME --password $APIKEY
# helm3 repo update
helm3 upgrade $SERVICE_NAME-$NAMESPACE gbs-ncid-tech-team-helm-virtual/$SERVICE_NAME \
    --version $VERSION --install --wait --debug --dry-run \
    --namespace $NAMESPACE \
    -f values/common.yaml \
    -f values/$SERVICE_NAME.common.yaml \
    -f values/$NAMESPACE/common.yaml \
    -f values/$NAMESPACE/$SERVICE_NAME.yaml
